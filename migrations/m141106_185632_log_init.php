<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

use yii\db\Migration;

/**
 * Class m141106_185632_log_init
 */
class m141106_185632_log_init extends Migration
{
    private $logTableName = 'log';

    public function up()
    {
        $this->createTable($this->logTableName, [
            'id'       => $this->bigPrimaryKey(),
            'level'    => $this->integer(),
            'category' => $this->string(),
            'log_time' => $this->double(),
            'prefix'   => $this->text(),
            'message'  => $this->text(),
        ]);

        $this->createIndex('idx_log_level', $this->logTableName, 'level');
        $this->createIndex('idx_log_category', $this->logTableName, 'category');
    }

    public function down()
    {
        $this->dropTable($this->logTableName);
    }
}
