<?php

use emilasp\site\common\models\Page;
use emilasp\variety\models\Variety;
use yii\db\Migration;

/** ./yii migrate --migrationPath=./vendor/emilasp/yii2-site/migrations/
 * Class m151114_220359_AddTablePage
 */
class m151114_220359_AddTablePage extends Migration
{
    private $tableOptions = null;

    public function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
    }

    public function up()
    {
        $this->createTable('site_page', [
            'id'          => $this->primaryKey(11),
            'type'        => $this->smallInteger(1)->notNull(),
            'name'        => $this->string(255)->notNull(),
            'text'        => $this->text()->notNull(),
            'status'      => $this->smallInteger(1)->notNull(),
            'created_at'  => $this->dateTime(),
            'updated_at'  => $this->dateTime(),
            'created_by'  => $this->integer(11),
            'updated_by'  => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_site_page_created_by',
            'site_page',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_site_page_updated_by',
            'site_page',
            'updated_by',
            'users_user',
            'id'
        );

        $this->addBasePages();

        $this->addIndexPage();
    }

    public function down()
    {
        $this->dropTable('site_page');
        return true;
    }

    /**
     * Добавляем базовые страницы
     */
    private function addBasePages():void
    {
        $indexText = <<<HTML
          <p>Текст на главную</p>
          
          <div class="page-head">
               <h3 itemprop="headline name"><i class="fas fa-clone"></i> Свежие записи</h3>
               <div class="stripe-line"></div>
          </div>
          <relatedArticles />
          
          <hr />
          
          <div class="page-head">
               <h3 itemprop="headline name"><i class="fas fa-clone"></i> Популярное</h3>
               <div class="stripe-line"></div>
          </div>
          <lastArticles limit="10" />  
HTML;



        $this->addPage('Главная', $indexText);
        $this->addPage('О проекте', 'Текст: О проекте');
        $this->addPage('Контакты', 'Текст: Контакты');
        $this->addPage('Политика безопастности ', 'Текст: Политика безопастности');
    }

    /**
     * Add page
     *
     * @param string $name
     * @param string $text
     */
    private function addPage(string $name, string $text): void
    {
        $indexPage = new Page();
        $indexPage->type = 1;
        $indexPage->name = $name;
        $indexPage->text = $text;
        $indexPage->status = 1;
        $indexPage->created_by = 1;
        $indexPage->updated_by = 1;
        $indexPage->save();
    }

    private function addIndexPage()
    {
        Variety::add('page_type', 'page_type_base', 'Обычная', 1, 1);
    }
}
