<?php

namespace emilasp\site\common;

use emilasp\core\CoreModule;
use emilasp\settings\behaviors\SettingsBehavior;
use emilasp\settings\models\Setting;
use yii\helpers\ArrayHelper;

/**
 * Class SiteBaseModule
 * @package emilasp\site\common
 */
class SiteBaseModule extends CoreModule
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'setting' => [
                'class'    => SettingsBehavior::className(),
                'meta'     => [
                    'name' => 'Сайт',
                    'type' => Setting::TYPE_MODULE,
                ],
                'settings' => [
                    [
                        'code'        => 'site_enabled',
                        'name'        => 'Сайт включён',
                        'type'        => SettingsBehavior::TYPE_SELECT,
                        'description' => 'Включение/выключение сайта(заглушка)',
                        'default'     => 1,
                        'data'        => [
                            Setting::DEFAULT_SELECT_YES => 'Да',
                            Setting::DEFAULT_SELECT_NO  => 'Нет',
                        ],
                    ],
                    [
                        'code'        => 'site_name',
                        'name'        => 'Название сайта',
                        'description' => '',
                    ],
                    [
                        'code'        => 'site_description',
                        'name'        => 'Описание сайта',
                        'description' => '',
                    ],
                    [
                        'code'        => 'site_phone',
                        'name'        => 'Телефон проекта',
                        'description' => '',
                    ],
                    [
                        'code'        => 'site_email',
                        'name'        => 'Email проекта',
                        'description' => '',
                    ],
                    [
                        'code'        => 'site_address',
                        'name'        => 'Адрес проекта',
                        'description' => '',
                    ],


                    [
                        'code'        => 'index_page',
                        'name'        => 'Главная страница сайта',
                        'description' => 'ID страницы которая будет показываться на главной',
                        'default'     => 1,
                    ],
                    [
                        'code'        => 'page_cache_duration',
                        'name'        => 'Кеширования страниц',
                        'description' => 'Время кеширования страниц',
                        'default'     => 16,
                    ],
                ],
            ],
        ], parent::behaviors());
    }
}
