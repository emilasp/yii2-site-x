<?php

namespace emilasp\site\common\extensions\breadcrumbs;

use emilasp\core\helpers\StringHelper;
use yii;
use yii\base\InvalidConfigException;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/**
 * Расширение для вывода хлебных крошек
 *
 * Class Breadcrumbs
 * @package emilasp\site\common\extensions\breadcrumbs
 */
class Breadcrumbs extends \yii\base\Widget
{
    public $first;
    public $flat;

    public $items;

    /**
     * @var string the name of the breadcrumb container tag.
     */
    public $tag = 'ul';
    /**
     * @var array the HTML attributes for the breadcrumb container tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = ['class' => 'breadcrumb', 'xmlns:v' => 'http://rdf.data-vocabulary.org/#'];
    /**
     * @var bool whether to HTML-encode the link labels.
     */
    public $encodeLabels = true;
    /**
     * @var array the first hyperlink in the breadcrumbs (called home link).
     * Please refer to [[links]] on the format of the link.
     * If this property is not set, it will default to a link pointing to [[\yii\web\Application::homeUrl]]
     * with the label 'Home'. If this property is false, the home link will not be rendered.
     */
    public $homeLink = false;
    /**
     * @var array list of links to appear in the breadcrumbs. If this property is empty,
     * the widget will not render anything. Each array element represents a single link in the breadcrumbs
     * with the following structure:
     *
     * ```php
     * [
     *     'label' => 'label of the link',  // required
     *     'url' => 'url of the link',      // optional, will be processed by Url::to()
     *     'template' => 'own template of the item', // optional, if not set $this->itemTemplate will be used
     * ]
     * ```
     *
     * If a link is active, you only need to specify its "label", and instead of writing `['label' => $label]`,
     * you may simply use `$label`.
     *
     * Since version 2.0.1, any additional array elements for each link will be treated as the HTML attributes
     * for the hyperlink tag. For example, the following link specification will generate a hyperlink
     * with CSS class `external`:
     *
     * ```php
     * [
     *     'label' => 'demo',
     *     'url' => 'http://example.com',
     *     'class' => 'external',
     * ]
     * ```
     *
     * Since version 2.0.3 each individual link can override global [[encodeLabels]] param like the following:
     *
     * ```php
     * [
     *     'label' => '<strong>Hello!</strong>',
     *     'encode' => false,
     * ]
     * ```
     */
    public $links = [];
    /**
     * @var string the template used to render each inactive item in the breadcrumbs. The token `{link}`
     * will be replaced with the actual HTML link for each inactive item.
     */
    public $itemParams = ['class' => 'breadcrumb-item'];
    /**
     * @var string the template used to render each active item in the breadcrumbs. The token `{link}`
     * will be replaced with the actual HTML link for each active item.
     */
    public $activeItemParams = ['class' => 'breadcrumb-item active'];


    /**
     * INIT
     */
    public function init()
    {
        $this->updateItems();

        $this->registerAssets();
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        if (empty($this->items)) {
            return;
        }
        $links = [];

        $this->items = array_values($this->items);
        //array_unshift($this->items, ['label' => Yii::t('yii', 'Home'), 'url'   => Yii::$app->homeUrl]);

        $countItems = count($this->items);
        foreach ($this->items as $index => $link) {
            if (!is_array($link)) {
                $link = ['label' => $link, 'itemprop' => 'url'];
            }
            
            $params = isset($link['url']) ? $this->itemParams : $this->activeItemParams;

            if (!isset($link['url'])) {
                $link['url'] = Yii::$app->request->absoluteUrl;
            }

            $links[] = $this->renderItem(
                $link,
                $params,
                $this->getParams($index, $countItems)
            );
        }
        echo Html::tag($this->tag, implode('', $links), $this->options);
    }

    /**
     * Формируем параметры для
     *
     * @param int $index
     * @param int $count
     * @return array
     */
    private function getParams(int $index, int $count): array
    {
        $params = [
            'id'        => 'breadcrumb-' . $index,
            'itemscope' => 'itemscope',
            'itemtype'  => 'http://data-vocabulary.org/Breadcrumb',
            'typeof'    => 'v:Breadcrumb'
        ];

        if ($index + 1 < $count) {
            $params['itemref'] = 'breadcrumb-' . ($index + 1);
        }

        return $params;
    }

    /**
     * Renders a single breadcrumb item.
     * @param array  $link the link to be rendered. It must contain the "label" element. The "url" element is optional.
     * @param string $template the template to be used to rendered the link. The token "{link}" will be replaced by the
     *     link.
     * @return string the rendering result
     * @throws InvalidConfigException if `$link` does not have "label" element.
     */
    protected function renderItem($link, $params, array $templateParams)
    {
        $template = Html::tag('li', '{link}', ArrayHelper::merge($templateParams, $params));

        $encodeLabel = ArrayHelper::remove($link, 'encode', $this->encodeLabels);
        if (array_key_exists('label', $link)) {
            $label = $encodeLabel ? Html::encode($link['label']) : $link['label'];
        } else {
            throw new InvalidConfigException('The "label" element is required for each link.');
        }
        if (isset($link['template'])) {
            $template = $link['template'];
        }
        if (isset($link['url'])) {
            $options = $link;
            unset($options['template'], $options['label'], $options['url']);
            $options['itemprop'] = 'url title';
            $options['rel']      = 'v:url';
            //$options['itemprop'] = '';
            $options['property'] = 'v:title';

            $link = Html::a($label, $link['url'], $options);
        } else {
            $link = $label;
        }
        return strtr($template, ['{link}' => $link]);
    }


    /**
     * Register client assets
     */
    public function registerAssets()
    {
        $view = $this->getView();
        BreadcrumbsAsset::register($view);
    }

    /**
     * Update items
     */
    private function updateItems(): void
    {
        $homeLink = '/';

        $this->items = ArrayHelper::merge([
            'first' => [
                'url'   => Url::toRoute($homeLink),
                'label' => Yii::t('site', 'Home'),
            ],
        ], $this->items);

        foreach ($this->items as $index => $item) {
            if (isset($item['label'])) {
                $this->items[$index]['label'] = StringHelper::truncate($item['label'], 35);
            } elseif (is_string($item)) {
                $this->items[$index] = StringHelper::truncate($item, 35);
            }
        }
    }
}
