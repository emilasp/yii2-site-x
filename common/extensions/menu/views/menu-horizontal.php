<?php
use kartik\nav\NavX;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

$navBarOptions = [
    'options'               => [
        'class' => 'navbar fixed-top navbar-expand-sm navbar-dark bg-dark',//navbar fixed-top navbar-expand-sm navbar-dark bg-dark
        //'id'    => 'topMenu',
    ],
    'containerOptions' => ['class' => 'navbar-toggleable-sm']
];


NavBar::begin($navBarOptions);
echo ' <a class="navbar-brand" href="/">
        <img alt="Brand" src="/images/logo.png">
      </a>';
foreach ($menus as $menu) {
    $_menu = [
        'activateParents' => true,
        'encodeLabels'    => false,
    ];

    if (isset($menu['options'])) {
        $_menu['options'] = $menu['options'];
    }
    if (isset($menu['items'])) {
        $_menu['items'] = $menu['items'];
    }

    //@TODO
    echo Nav::widget($_menu);
}


if (!empty($menusRight)) {
    foreach ($menusRight as $menu) {
        $_menu = [
            'activateParents' => true,
            'encodeLabels'    => false,
        ];

        if (isset($menu['options'])) {
            $_menu['options'] = $menu['options'];
        }
        if (isset($menu['items'])) {
            $_menu['items'] = $menu['items'];
        }

        //@TODO
        echo Nav::widget($_menu);
    }
}

NavBar::end();


