<?php

use kartik\nav\NavX;
use kartik\widgets\SideNav;

$navBarOptions = [
    'brandLabel' => '<span class="logo-amulex"></span> ',
    'options'    => [
        'class' => 'navbar navbar-inverse navbar-static-top',
        'id'    => 'topMenu',
    ],
];

foreach ($menus as $menu) {

    if (isset($menu['items'])) {
        echo SideNav::widget([
            'type'         => SideNav::TYPE_SUCCESS,
            'encodeLabels' => false,
            'heading'      => Yii::t('users', 'Menu'),
            'items'        => $menu['items']
        ]);
    }

}



