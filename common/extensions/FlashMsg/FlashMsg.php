<?php

namespace emilasp\site\common\extensions\FlashMsg;

use Yii;
use yii\base\Widget;

/**
 * Class FlashMsg
 * @package emilasp\site\extensions\FlashMsg
 */
class FlashMsg extends Widget
{
    public static $typeColors = [
        'success' => 'green',
        'info'    => 'grey',
        'primary' => 'blue',
        'warning' => 'orange',
        'danger'  => 'red',
    ];

    public function run()
    {
        $js        = '';
        $messages  = Yii::$app->session->getAllFlashes();
        $delayOpen = 0;
        foreach ($messages as $type => $typeMessages) {
            if (is_array($typeMessages)) {
                foreach ($typeMessages as $message) {
                    $this->addNoticeJs($js, $type, $message, $delayOpen);
                }
            } else {
                $this->addNoticeJs($js, $type, $typeMessages, $delayOpen);
            }
        }
        $this->view->registerJs($js);
    }

    /**
     * @param string $js
     * @param string $type
     * @param string $message
     * @param int    $delayOpen
     */
    private function addNoticeJs(string &$js, string $type, string $message, int &$delayOpen)
    {
        $js        .= '
                setTimeout(function(){
                    new jBox("Notice", {
                            "id":"notice' . $delayOpen . '",
                            "content":"' . $message . '",
                            "position":{"x":"right","y":"top"},
                            "color": "' . self::$typeColors[$type] . '",
                            "animation":"tada"
                        });
                },
                ' . $delayOpen . ');
            ';
        $delayOpen += 1000;
    }
}
