<?php
namespace emilasp\site\common\extensions\UserHistory;

use yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\Cookie;

/**
 * Расширение для вывода истории посещения страниц
 *
 * Class UserHistory
 * @package emilasp\site\common\extensions\UserHistory
 */
class UserHistory extends \yii\base\Widget
{
    const COOKIE_HISTORY_NAME_PREFIX = 'history-user:';
    const LIMIT_COOKIE_COUNT         = 9;

    private $items = [];

    /**
     * INIT
     */
    public function init()
    {
        $this->registerAssets();
        $this->setItemsCookie();
    }

    /**
     * RUN
     */
    public function run()
    {
        echo $this->render('user-history', ['items' => $this->items]);
    }

    /**
     * Register client assets
     */
    public function registerAssets()
    {
        UserHistoryAsset::register($this->getView());
    }

    /**
     * Set items from Cookie
     */
    private function setItemsCookie()
    {
        if (!Yii::$app->user->isGuest) {
            $cookieName = self::COOKIE_HISTORY_NAME_PREFIX . Yii::$app->user->id;


            $this->items = json_decode(Yii::$app->request->cookies->getValue($cookieName, '[]'), true);
            $countItems  = count($this->items) - 1;
            $lastItem    = ArrayHelper::getValue($this->items, $countItems, [
                'title'  => '',
                'url'    => '',
                'action' => '',
                'count'  => '',
                'date'   => ''
            ]);

            $newItem = [
                'title'  => $this->view->title,
                'url'    => Url::current(),
                'action' => Yii::$app->controller->action->id,
                'count'  => 1,
                'date'   => date('d H:i:s')
            ];

            if ($newItem['url'] !== $lastItem['url']) {
                $this->items[] = $newItem;
                if ($countItems + 2 > self::LIMIT_COOKIE_COUNT) {
                    unset($this->items[0]);
                }
            } else {
                $lastItem['count']++;
                $this->items[$countItems] = $lastItem;
            }

            Yii::$app->response->cookies->add(new Cookie([
                'name'  => $cookieName,
                'value' => json_encode($this->items, JSON_UNESCAPED_UNICODE)
            ]));

            $this->items = array_reverse($this->items);

            $this->setIcon();
        }
    }

    /**
     * Устанавлоиваем классы иконок
     */
    private function setIcon()
    {
        foreach ($this->items as $index => $item) {
            switch ($item['action']) {
                case 'index':
                    $this->items[$index]['iconClass'] = 'fa fa-list text-primary';
                    break;
                case 'view':
                    $this->items[$index]['iconClass'] = 'fa fa-eye text-primary';
                    break;
                case 'create':
                    $this->items[$index]['iconClass'] = 'fa fa-plus-square text-success';
                    break;
                case 'update':
                    $this->items[$index]['iconClass'] = 'fa fa-pencil-square text-success';
                    break;
                case 'error':
                    $this->items[$index]['iconClass'] = 'fa fa-exclamation-triangle text-danger';
                    break;
                default:
                    $this->items[$index]['iconClass'] = 'fa fa-chrome text-muted';
            }
        }
    }
}
