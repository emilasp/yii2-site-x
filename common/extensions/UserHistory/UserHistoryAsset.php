<?php
namespace emilasp\site\common\extensions\UserHistory;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Class UserHistoryAsset
 * @package emilasp\site\common\extensions\UserHistory
 */
class UserHistoryAsset extends AssetBundle
{
    public $jsOptions = ['position' => View::POS_HEAD];

    public $sourcePath = __DIR__ . '/assets';

    public $css = ['user-history.css'];
}
