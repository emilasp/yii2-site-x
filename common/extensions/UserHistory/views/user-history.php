<?php
use yii\helpers\Html;
?>
<div id="history-panel">
    <i class="fa fa-history"></i> <?= Yii::t('site', 'User history') ?>
    <span class="text-muted"><small>(<?= count($items) ?>)</small></span>
    <div id="history-panel-container">
        <?php foreach ($items as $index => $item) : ?>
            <div class="row">
                <div class="col-md-1 text-warning"><?= $index + 1 ?></div>
                <div class="col-md-8 history-title">
                    <i class="<?= $item['iconClass'] ?>"></i>
                    <?= Html::a(strip_tags($item['title']), $item['url'], []) ?>
                    <span class="text-muted"><small>(<?= $item['count'] ?>)</small></span>
                </div>

                <div class="col-md-3 text-muted"><span class="text-warning"><?= $item['date'] ?></span></div>
            </div>
        <?php endforeach; ?>
    </div>
</div>>