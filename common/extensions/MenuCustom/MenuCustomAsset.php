<?php
namespace emilasp\site\common\extensions\MenuCustom;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Class MenuCustomAsset
 * @package emilasp\site\common\extensions\MenuCustom
 */
class MenuCustomAsset extends AssetBundle
{
    public $jsOptions = ['position' => View::POS_END];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];

    public $sourcePath = __DIR__ . '/assets';


    public $css = [
        'menu-custom.css',
    ];

    public $js = [
        'menu-custom.js'
    ];
}
