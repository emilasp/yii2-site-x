<?php

use yii\helpers\Html;
use yii\helpers\Url;


$items = \yii\helpers\ArrayHelper::merge($itemsLeft, $itemsRight);
?>

<div class="menu-container">
    <div class="">

        <div class="row">
            <div class="col-md-12">
                <ul id="menu">

                    <?= Html::beginTag('li', ['class' => 'logo']) ?>

                    <a href="/"><img src="/images/logo.png"/></a>

                    <?= Html::endTag('li') ?>

                    <?php foreach ($items as $item) : ?>

                        <?php if (!isset($item['visible']) || $item['visible']) : ?>

                            <?php if (isset($item['label'])) : ?>

                                <?= Html::beginTag('li', ['class' => $item['class'] ?? '']) ?>

                                <?= Html::a(
                                    (isset($item['icon']) ? Html::tag('i', '', ['class' => $item['icon']]) . ' ' : '')
                                    . $item['label'],
                                    Url::toRoute($item['url']),
                                    ($item['linkOptions'] ?? [])
                                ) ?>

                                <?php if (isset($item['items'])) : ?>

                                    <?= Html::beginTag('ul') ?>

                                    <?php foreach ($item['items'] as $subItem) : ?>

                                        <?php if (!isset($subItem['visible']) || $subItem['visible']) : ?>

                                            <?= Html::beginTag('li') ?>

                                            <?php if (isset($subItem['label'])) : ?>
                                                <?= Html::a(
                                                    (isset($subItem['icon']) ? Html::tag('i', '',
                                                            ['class' => $subItem['icon']])
                                                        . ' ' : '')
                                                    . $subItem['label'],
                                                    Url::toRoute($subItem['url']),
                                                    ($item['linkOptions'] ?? [])
                                                ) ?>
                                            <?php endif; ?>

                                            <?= Html::endTag('li') ?>

                                        <?php endif; ?>

                                    <?php endforeach; ?>

                                    <?= Html::endTag('ul') ?>

                                <?php endif; ?>

                                <?= Html::endTag('li') ?>

                            <?php endif; ?>
                        <?php endif; ?>

                    <?php endforeach; ?>

                </ul>
            </div>

        </div>
    </div>

</div>
