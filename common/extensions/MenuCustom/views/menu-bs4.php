<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<nav class="navbar sticky-top navbar-expand-lg navbar-dark">

        <a href="/" class="navbar-brand"><?= $brandHtml ?></a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">

                <?php foreach ($itemsLeft as $index => $item) : ?>

                    <?php if (isset($item['label'])) : ?>

                        <?php if (!isset($item['visible']) || $item['visible']) : ?>

                            <?php if (!isset($item['items'])) : ?>

                                <?= Html::beginTag('li', ['class' => 'nav-item']) ?>

                                <?= Html::a(
                                    (isset($item['icon']) ? Html::tag('i', '', ['class' => $item['icon']]) . ' ' : '')
                                    . $item['label'],
                                    Url::toRoute($item['url']),
                                    ArrayHelper::merge(['class' => 'nav-link'], ($item['options'] ?? []))
                                ) ?>

                                <?= Html::endTag('li') ?>


                            <?php else : ?>

                                <?= Html::beginTag('li', ['class' => 'nav-item dropdown']) ?>

                                <?php if (isset($item['label'])) : ?>

                                    <?= Html::a(
                                        (isset($item['icon']) ? Html::tag('i', '',
                                                ['class' => $item['icon']])
                                            . ' ' : '')
                                        . $item['label'],
                                        '#',
                                        [
                                            'id'            => 'sub-' . $index,
                                            'class'         => 'nav-link',
                                            'data-toggle'   => 'dropdown',
                                            'aria-haspopup' => 'true',
                                            'aria-expanded' => 'false',
                                        ]
                                    ) ?>

                                    <?= Html::beginTag('div', [
                                        'class'           => 'dropdown-menu',
                                        'aria-labelledby' => 'sub-' . $index,
                                    ]) ?>


                                    <?php foreach ($item['items'] as $subIndex => $subItem) : ?>

                                        <?php if (isset($subItem['label'])) : ?>

                                            <?php if (!isset($subItem['visible']) || $subItem['visible']) : ?>

                                                <?= Html::a(
                                                    (isset($subItem['icon']) ? Html::tag('i', '',
                                                            ['class' => $subItem['icon']]) . ' ' : '')
                                                    . $subItem['label'],
                                                    Url::toRoute($subItem['url']),
                                                    ['class' => 'dropdown-item']
                                                ) ?>

                                            <?php endif; ?>
                                        <?php else: ?>
                                            <? /*= $subItem */ ?>
                                        <?php endif; ?>

                                    <?php endforeach; ?>

                                    <?= Html::endTag('div') ?>

                                <?php else: ?>
                                    <?= $item ?>
                                <?php endif; ?>

                                <?= Html::endTag('li') ?>

                            <?php endif; ?>


                        <?php else : ?>
                            <?= $item ?>
                        <?php endif; ?>

                    <?php endif; ?>

                <?php endforeach; ?>

            </ul>


            <ul class="navbar-nav">

                <?php foreach ($itemsRight as $index => $item) : ?>

                    <?php if (!isset($item['visible']) || $item['visible']) : ?>

                        <?php if (isset($item['label'])) : ?>

                            <?php if (!isset($item['items'])) : ?>

                                <?= Html::beginTag('li', ['class' => 'nav-item']) ?>

                                <?= Html::a(
                                    (isset($item['icon']) ? Html::tag('i', '', ['class' => $item['icon']]) . ' ' : '')
                                    . $item['label'],
                                    ($item['url'] === '#' ? '#' : Url::toRoute($item['url'])),
                                    ArrayHelper::merge(['class' => 'nav-link'], ($item['options'] ?? []))
                                ) ?>

                                <?= Html::endTag('li') ?>


                            <?php else : ?>

                                <?= Html::beginTag('li', ['class' => 'nav-item dropdown']) ?>

                                <?php if (isset($item['label'])) : ?>
                                    <?= Html::a(
                                        (isset($item['icon']) ? Html::tag('i', '',
                                                ['class' => $item['icon']])
                                            . ' ' : '')
                                        . $item['label'],
                                        '#',
                                        [
                                            'id'            => 'sub-' . $index . '-' . $index,
                                            'class'         => 'nav-link',
                                            'data-toggle'   => 'dropdown',
                                            'aria-haspopup' => 'true',
                                            'aria-expanded' => 'false',
                                        ]
                                    ) ?>
                                <?php endif; ?>

                                <?= Html::beginTag('div', [
                                    'class'           => 'dropdown-menu dropdown-menu-right',
                                    'aria-labelledby' => 'sub-' . $index,
                                ]) ?>

                                <?php foreach ($item['items'] as $subIndex => $subItem) : ?>

                                    <?php if (!isset($subItem['visible']) || $subItem['visible']) : ?>

                                        <?= Html::a(
                                            (isset($subItem['icon']) ? Html::tag('i', '',
                                                    ['class' => $subItem['icon']]) . ' ' : '')
                                            . $subItem['label'],
                                            Url::toRoute($subItem['url']),
                                            ArrayHelper::merge(
                                                ($subItem['linkOptions'] ?? []),
                                                ['class' => 'dropdown-item']
                                            )
                                        ) ?>

                                    <?php endif; ?>

                                <?php endforeach; ?>

                                <?= Html::endTag('div') ?>

                                <?= Html::endTag('li') ?>

                            <?php endif; ?>


                        <?php endif; ?>

                    <?php endif; ?>

                <?php endforeach; ?>

            </ul>
        </div>

</nav>

