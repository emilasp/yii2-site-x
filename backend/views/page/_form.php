<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use emilasp\seo\widgets\SeoForm\SeoForm;
use emilasp\core\extensions\ImeraviEditor\ImperaviEditor;

/* @var $this yii\web\View */
/* @var $model emilasp\site\common\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <div class="col-md-2">
        <ul class="nav nav-pills flex-column">
            <li class="active">
                <a data-toggle="tab" href="#base" class="nav-link"><?= Yii::t('site', 'Tab Base') ?></a>
            </li>
            <li><a data-toggle="tab" href="#seo" class="nav-link"><?= Yii::t('site', 'Tab Seo') ?></a></li>
        </ul>
    </div>

    <div class="col-md-10">
        <div class="tab-content">
            <div id="base" class="tab-pane fade in active">
                <?= $form->field($model, 'type')->dropDownList($model->types) ?>
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'text')->widget(ImperaviEditor::class, [
                    'settings' => [
                        'imageUpload'       => Url::to(['/cms/article/image-upload']),
                        'imageManagerJson'  => Url::to([
                            '/cms/article/images-get',
                            'id'     => $model->id,
                            'object' => $model::className()
                        ]),
                        'uploadImageFields' => ['id' => $model->id, 'object' => $model::className()],
                    ],
                ]); ?>

                <?= $form->field($model, 'status')->dropDownList($model->statuses) ?>
            </div>
            <div id="seo" class="tab-pane fade">
                <?= SeoForm::widget(['form' => $form, 'model' => $model]) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
