<?php
use emilasp\users\common\models\User;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use emilasp\site\common\models\Page;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\site\common\models\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('site', 'Pages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => '\kartik\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'class'     => '\kartik\grid\DataColumn',
                'width'     => '100px',
                'hAlign'    => GridView::ALIGN_CENTER,
                'vAlign'    => GridView::ALIGN_MIDDLE,
            ],
            'name',
            [
                'attribute'           => 'created_by',
                'value'               => function ($model) {
                    return $model->createdBy->username;
                },
                'class'               => '\kartik\grid\DataColumn',
                'hAlign'              => GridView::ALIGN_LEFT,
                'vAlign'              => GridView::ALIGN_MIDDLE,
                'width'               => '150px',
                'filterType'          => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'language'      => \Yii::$app->language,
                    'data'          => ArrayHelper::map(User::find()->all(), 'id', 'username'),
                    'options'       => ['placeholder' => '-выбрать-'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
            ],
            [
                'attribute' => 'status',
                'value'     => function ($model, $key, $index, $column) {
                    return Page::$statuses[$model->status];
                },
                'class'     => '\kartik\grid\DataColumn',
                'hAlign'    => GridView::ALIGN_LEFT,
                'vAlign'    => GridView::ALIGN_MIDDLE,
                'width'     => '150px',
                'filter'    => Page::$statuses,
            ],
            [
                'attribute' => 'created_at',
                'class'     => '\kartik\grid\DataColumn',
                'hAlign'    => GridView::ALIGN_LEFT,
                'vAlign'    => GridView::ALIGN_MIDDLE,
                'width'     => '250px',
            ],
            [
                'attribute' => 'type',
                'value'     => function ($model, $key, $index, $column) {
                    return $model->types[$model->id] ?? null;
                },
                'class'     => '\kartik\grid\DataColumn',
                'hAlign'    => GridView::ALIGN_LEFT,
                'vAlign'    => GridView::ALIGN_MIDDLE,
                'width'     => '150px',
                'filter'    => $searchModel->types,
            ],
            [
                'class' => '\kartik\grid\ActionColumn',
            ],
        ],
        'responsive'   => true,
        'hover'        => true,
        'condensed'    => true,
        'floatHeader'  => true,
        'panel'        => [
            'heading'    => '<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> ' . Html::encode($this->title) . ' </h3>',
            'type'       => 'info',
            'before'     => Html::a(
                '<i class="glyphicon glyphicon-plus"></i> ' . Yii::t('site', 'Add'),
                ['create'],
                ['class' => 'btn btn-success']
            ),
            'after'      => Html::a(
                '<i class="glyphicon glyphicon-repeat"></i> Reset List',
                ['index'],
                ['class' => 'btn btn-info']
            ),
            'showFooter' => false,
        ],
    ]);
    ?>

</div>
