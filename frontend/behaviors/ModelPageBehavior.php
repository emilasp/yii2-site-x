<?php
namespace emilasp\site\frontend\behaviors;

use yii;
use yii\helpers\Url;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use emilasp\core\helpers\StringHelper;

/** Поведение формирует url для моделей, а так же редиректит, если он ошибочен или исправлен title(name)
 * Class ModelPageBehavior
 * @package emilasp\core\behaviors
 */
class ModelPageBehavior extends Behavior
{
    const DEFAULT_ACTION = 'view';

    public $moduleId;
    public $attrName        = 'name';
    public $attrTitle       = 'title';
    public $attrKeywords    = 'keywords';
    public $attrDescription = 'description';

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'saveModel',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'saveModel',
        ];
    }

    /**
     * Сохраняем для сео title и name есть одно из них заполнено
     */
    public function saveModel()
    {
        if (!$this->owner->{$this->attrName} || !$this->owner->{$this->attrTitle}) {
            if ($this->owner->{$this->attrName}) {
                $this->owner->{$this->attrTitle} = $this->owner->{$this->attrName};
            } elseif ($this->owner->{$this->attrTitle}) {
                $this->owner->{$this->attrName} = $this->owner->{$this->attrTitle};
            }
        }
    }

    /**
     *  Получаем url для страницы модели
     *
     * @param bool $title
     * @param bool $fullUrl - полный Url
     * @param bool string $action
     *
     * @return string
     */
    public function getUrl($title = false, $fullUrl = false, $action = '')
    {
        if (!$action) {
            $action = self::DEFAULT_ACTION;
        }
        $className = strtolower(StringHelper::basename($this->owner->className()));

        $route = '/' . $this->moduleId . '/' . $className . '/' . $action;
        if ($title !== false) {
            return Url::toRoute([$route, 'id' => $this->owner->id, 'title' => $title], $fullUrl);
        }

        return Url::toRoute([
            $route,
            'id'    => $this->owner->id,
            'title' => StringHelper::str2url($this->owner->{$this->attrTitle}),
        ], $fullUrl);
    }

    /**
     *  Проверяем текущий url на валидность и делаем редирект или выкидываем исключение
     *
     * @param string|bool $url
     * @param bool|string $action
     *
     * @return bool|string
     * @throws \yii\web\NotFoundHttpException
     */
    public function checkUrl($url = false, $action = '')
    {
        $urlModel = $this->owner->getUrl(false, false, $action);

        if (!$url) {
            $url = Yii::$app->request->getUrl(true);
            if (strpos($url, '.html?')) {
                $arrUrl = explode('.html?', $url);
                $url    = $arrUrl[0] . '.html';
            }
        }
        if ($urlModel == $url) {
            return $url;
        } else {
            Yii::$app->controller->redirect(Url::to($urlModel), 301);
        }
    }

    /** Проверяем Url на валидность(защита от дублей) и регистрируем мета
     *
     * @param bool|true $checkUrl
     * @param bool|true $addMeta
     */
    public function setPage($checkUrl = true, $addMeta = true)
    {
        if ($checkUrl) {
            $this->checkUrl();
        }
        if ($addMeta) {
            $this->addMetaTags();
        }
    }

    /**
     * Регистрируем мета для поисковиков
     */
    private function addMetaTags()
    {
        $title       = $this->owner->{$this->attrTitle};
        $keywords    = $this->owner->{$this->attrKeywords};
        $description = $this->owner->{$this->attrDescription};

        Yii::$app->controller->view->title = $title;
        Yii::$app->controller->view->registerMetaTag(['name' => 'keywords', 'content' => $keywords]);
        Yii::$app->controller->view->registerMetaTag(['name' => 'description', 'content' => $description]);
    }
}
