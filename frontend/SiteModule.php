<?php
namespace emilasp\site\frontend;

use emilasp\site\common\SiteBaseModule;

/**
 * Class SiteModule
 * @package emilasp\site\frontend
 */
class SiteModule extends SiteBaseModule
{
    public $menu;
}
