<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = $model->seo->meta_title;

$this->params['breadcrumbs'][] = $this->title;
?>



<article itemscope itemtype="http://schema.org/Article">
    <header>

        <div class="page-head">
            <h1 itemprop="headline name"><?= $model->seo->h1 ?></h1>
            <div class="stripe-line"></div>
        </div>

        <meta itemscope itemprop="mainEntityOfPage" itemType="https://schema.org/WebPage"
              itemid="<?= $model->getUrl(null, true) ?>"/>


    </header>

    <div itemprop="articleBody">
        <?= $model->text ?>
    </div>

</article>

<?php $this->registerJs('new jBox("Image")'); ?>

