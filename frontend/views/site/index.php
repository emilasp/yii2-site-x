<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title                   = $model->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page">

    <?= Html::decode($model->text) ?>

</div>
