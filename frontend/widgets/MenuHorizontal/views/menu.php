<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;


$items = \yii\helpers\ArrayHelper::merge($itemsLeft, $itemsRight);
?>

<nav class="navbar-expand-lg">

    <button class="navbar-toggler" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false" aria-label="Toggle navigation"><i class="fa fa-bars"></i>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">


        <ul class="navbar-nav">

            <?php foreach ($itemsLeft as $index => $item) : ?>
                <?php if (isset($item['label'])) : ?>
                    <?php if (!isset($item['visible']) || $item['visible']) : ?>
                        <?= Html::beginTag('li', ['class' => 'nav-item']) ?>
                        <?= Html::a(
                            (isset($item['icon']) ? Html::tag('i', '',
                                    ['class' => $item['icon']]) . ' ' : '')
                            . $item['label'],
                            Url::toRoute($item['url']),
                            ArrayHelper::merge(['class' => 'nav-link'], ($item['options'] ?? []))
                        ) ?>

                        <?php if (isset($item['items'])) : ?>
                            <ul>
                                <?php foreach ($item['items'] as $subIndex => $subItem) : ?>
                                    <?php if (isset($subItem['label'])) : ?>
                                        <?php if (!isset($subItem['visible']) || $subItem['visible']) : ?>
                                            <?= Html::beginTag('li', ['class' => 'nav-item']) ?>
                                            <?= Html::a(
                                                (isset($subItem['icon']) ? Html::tag('i', '',
                                                        ['class' => $subItem['icon']]) . ' ' : '')
                                                . $subItem['label'],
                                                Url::toRoute($subItem['url']),
                                                ['class' => '']
                                            ) ?>
                                            <?= Html::endTag('li') ?>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <? /*= $subItem */ ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>

                        <?= Html::endTag('li') ?>


                    <?php endif; ?>
                <?php else : ?>
                    <?= $item ?>
                <?php endif; ?>
            <?php endforeach; ?>

        </ul>

    </div>
</nav>




