<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

?>
    <nav class="navbar navbar-expand-lg">
        <button class="navbar-toggler" type="button"
                data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="fa fa-bars color-white"></span>
            Меню
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul itemscope itemtype="http://www.schema.org/SiteNavigationElement" class="navbar-nav mr-auto">

                <?php foreach ($itemsLeft as $index => $item) : ?>

                    <?php if (isset($item['label'])) : ?>

                        <?php if (!isset($item['visible']) || $item['visible']) : ?>

                            <?php if (!isset($item['items'])) : ?>

                                <?= Html::beginTag('li', ['itemprop' => 'name', 'class' => 'nav-item']) ?>

                                <?= Html::a(
                                    (isset($item['icon']) ? Html::tag('i', '', ['class' => $item['icon']]) . ' ' : '')
                                    . $item['label'],
                                    Url::to($item['url'], false),
                                    ArrayHelper::merge(['itemprop' => 'url'], ($item['options'] ?? []))
                                ) ?>

                                <?= Html::endTag('li') ?>


                            <?php else : ?>

                                <?= Html::beginTag('li', ['itemprop' => 'name', 'class' => 'nav-item mainMenuDropdown']) ?>

                                <?php if (isset($item['label'])) : ?>

                                    <?= Html::a(
                                        (isset($item['icon']) ? Html::tag('i', '',
                                                ['class' => $item['icon']])
                                            . ' ' : '')
                                        . $item['label'],
                                        '#',
                                        [
                                            'itemprop' => 'url',
                                            'class'    => 'mainMenuDropdownA',
                                        ]
                                    ) ?>

                                    <?= Html::beginTag('ul', ['class' => 'mainMenuDropdownUl']) ?>


                                    <?php foreach ($item['items'] as $subIndex => $subItem) : ?>

                                        <?php if (isset($subItem['label'])) : ?>

                                            <?php if (!isset($subItem['visible']) || $subItem['visible']) : ?>

                                                <?= Html::beginTag('li', ['itemprop' => 'name']) ?>

                                                <?= Html::a(
                                                    (isset($subItem['icon']) ? Html::tag('i', '',
                                                            ['class' => $subItem['icon']]) . ' ' : '')
                                                    . $subItem['label'],
                                                    Url::to($subItem['url'], false),
                                                    ['itemprop' => 'url']
                                                ) ?>

                                                <?= Html::endTag('li') ?>

                                            <?php endif; ?>
                                        <?php else: ?>
                                            <? /*= $subItem */ ?>
                                        <?php endif; ?>

                                    <?php endforeach; ?>

                                    <?= Html::endTag('ul') ?>

                                <?php else: ?>
                                    <?= $item ?>
                                <?php endif; ?>

                                <?= Html::endTag('li') ?>

                            <?php endif; ?>


                        <?php else : ?>
                            <?= $item ?>
                        <?php endif; ?>

                    <?php endif; ?>

                <?php endforeach; ?>

            </ul>
        </div>
    </nav>


<?php
/*$js = <<<JS
JS;

$this->registerJs($js);*/