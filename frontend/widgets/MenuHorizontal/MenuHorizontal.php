<?php

namespace emilasp\site\frontend\widgets\MenuHorizontal;

use emilasp\core\components\base\Widget;
use yii;
use yii\helpers\Url;

/**
 * Class MenuHorizontal
 * @package emilasp\site\frontend\widgets\MenuHorizontal
 */
class MenuHorizontal extends Widget
{
    const CACHE_PREFIX = 'user_menu:';

    public $menuPath    = '@app/config/menu/';
    public $menuName    = 'menu-top';

    public $template    = 'menu';

    private $action;
    private $controller;
    private $module;
    private $userId;

    private $decompositionUrl;

    /**
     * INIT
     */
    public function init()
    {
        $this->registerAssets();

        $this->module     = Yii::$app->controller->module->id;
        $this->controller = Yii::$app->controller->id;
        $this->action     = Yii::$app->controller->action->id;

        $this->userId     = (!Yii::$app->user->isGuest) ? Yii::$app->user->id : null;
    }

    /**
     * RUN
     */
    public function run()
    {
        $items = include(Yii::getAlias($this->menuPath . $this->menuName . '.php'));
        $items = $this->removeByRight($items);

        $itemsRight = include(Yii::getAlias($this->menuPath . $this->menuName . '-right.php'));
        $itemsRight = $this->removeByRight($itemsRight);

        foreach ($itemsRight as $index => $item) {
            $itemsRight[$index]['class'] = 'float-md-right';
        }

        //$items = $this-> setActiveItems( $items );

        echo $this->render($this->template, [
            'id'         => $this->id,
            'itemsLeft'  => $items,
            'itemsRight' => $itemsRight,
        ]);
    }

    /**
     * Register client assets
     */
    public function registerAssets()
    {
        $view = $this->getView();
        MenuHorizontalAsset::register($view);
    }


    /** Убираем из списка меню все элементы не проходящие по правам
     *
     * @param $items
     *
     * @return mixed
     */
    private function removeByRight($items)
    {
        foreach ($items as $index => $item) {
            if (!is_array($item)) {
                continue;
            }

            $items[$index]['active'] = $this->isActive($items[$index]['url']);

            if (isset($items[$index]['url'])) {
                $items[$index]['url'] = ($item['url'] === '#' ? '#' : Url::toRoute($item['url']));
            }

            if (is_array($item)) {
                if (isset($item['items'])) {
                    $items[$index]['items'] = $this->removeByRight($item['items']);

                    if (count($items[$index]['items']) == 0) {
                        unset($items[$index]);
                    }
                } else {
                    if (isset($item['role'])) {
                        $isAllowRole = false;
                        foreach ((array)$item['role'] as $role) {
                            if ($role === '@') {
                                $isAllowRole = !Yii::$app->user->isGuest;
                            } elseif ($role === '?') {
                                $isAllowRole = Yii::$app->user->isGuest;
                            } elseif (Yii::$app->user->can($role)) {
                                $isAllowRole = true;
                            }
                        }
                        if (!$isAllowRole) {
                            unset($items[$index]);
                        }
                    }

                    if (isset($item['rule']) && is_callable($item['rule'])) {
                        $rule = $item['rule'];
                        if ($rule() !== true) {
                            unset($items[$index]);
                        }
                    }
                }
            }
        }
        return $items;
    }

    /**
     * Is active
     *
     * @param $route
     * @return bool
     */
    private function isActive($route)
    {
        $route = $this->decompositionRoute($route);

        if (Yii::$app->controller->module->id === $route['module']) {
            if (Yii::$app->controller->id === $route['controller'] || $route['controller'] === null) {
                if (Yii::$app->controller->action->id === $route['action'] || !$route['action']) {
                    return true;
                }
            }
        }
        return false;
    }

    /** Получаем наименования модуля, контроллера и экшена
     *
     * @param $route
     *
     * @return array
     */
    private function decompositionRoute($route)
    {
        //if (!$this->decompositionUrl) {
        $data = [
            'module'     => null,
            'controller' => null,
            'action'     => null,
        ];

        if (strpos($route, '/') !== false) {
            $route = substr($route, 1);
        }
        $dataRoute = explode('/', $route);
        $count     = count($dataRoute);
        for ($i = 0; $i < $count; $i++) {
            switch ($i) {
                case 0:
                    $data['module'] = $dataRoute[$i];
                    break;
                case 1:
                    $data['controller'] = $dataRoute[$i];
                    break;
                case 2:
                    $data['action'] = $dataRoute[$i];
                    break;
            }
        }
        $this->decompositionUrl = $data;
        //}

        return $this->decompositionUrl;
    }
}
