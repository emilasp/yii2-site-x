<?php
namespace emilasp\site\frontend\widgets\MenuHorizontal;

use yii\web\AssetBundle;

/**
 * Class MenuHorizontalAsset
 * @package emilasp\site\frontend\widgets\MenuHorizontal
 */
class MenuHorizontalAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $css = [
        'menu-horizontal.css',
    ];
}
