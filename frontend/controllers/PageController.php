<?php

namespace emilasp\site\frontend\controllers;

use emilasp\seo\behaviors\SeoMetaFilter;
use emilasp\seo\models\SeoData;
use Yii;
use emilasp\site\common\models\Page;
use emilasp\core\components\base\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'seo' => [
                'class'   => SeoMetaFilter::className(),
                'actions' => [
                    'view' => ['className' => Page::class, 'route' => '/cms/content/view'],
                ]
            ],
            [
                'class'      => 'yii\filters\PageCache',
                'only'       => ['index'],
                'duration'   => $this->module->getSetting('page_cache_duration'),
                'enabled'    => Yii::$app->user->isGuest && !YII_DEBUG,
                'dependency' => [
                    'class' => 'yii\caching\DbDependency',
                    'sql'   => 'SELECT MAX(site_page.updated_at) FROM site_page',
                ],
            ],
        ];
    }


    /**
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id, Page::class);
        $model->applyShortCodes();

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @param string $title
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionViewByTitle()
    {
        $title = Yii::$app->request->get('title');

        if (!$seo = SeoData::findOne(['link' => $title])) {
            return Yii::$app->runAction('/error');
        }

        return $this->render('view', [
            'model' => $seo->model,
        ]);
    }
}
