<?php

namespace emilasp\site\frontend\controllers;

use emilasp\core\components\base\Controller;
use emilasp\seo\components\SiteMapComponent;
use Yii;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * SitemapController
 */
class SitemapController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow'   => true,
                        'roles'   => ['?'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Render SiteMap.xml
     * @return mixed
     */
    public function actionIndex()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers                    = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/xml');

        $data = Yii::$app->sitemap->getSiteMapData();

        $sitemapData = $this->renderPartial('sitemap', ['data' => $data]);

        /*if ($module->enableGzip) {
            $sitemapData = gzencode($sitemapData);
            $headers->add('Content-Encoding', 'gzip');
            $headers->add('Content-Length', strlen($sitemapData));
        }*/

        return $sitemapData;
    }
}
