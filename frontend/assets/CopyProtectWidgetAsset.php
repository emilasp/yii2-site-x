<?php

namespace emilasp\site\frontend\assets;

use emilasp\core\components\base\AssetBundle;


/**
 * Class CopyProtectWidgetAsset
 * @package emilasp\site\frontend\assets
 */
class CopyProtectWidgetAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';
    public $js         = ['copy-protect-widget'];
}
