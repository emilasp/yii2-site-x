jQuery(document).on('copy', function(e)
{
    var sel = window.getSelection();
    var copyFooter =
        "<br /><br /> Источник: <a href='" + document.location.href + "'>" + document.location.href + "</a><br />© ПРАВОФОР";
    var copyHolder = $('<div>', {html: sel+copyFooter, style: {position: 'absolute', left: '-99999px'}});
    $('body').append(copyHolder);
    sel.selectAllChildren( copyHolder[0] );
    window.setTimeout(function() {
        copyHolder.remove();
    },0);
});